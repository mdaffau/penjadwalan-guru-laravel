<!DOCTYPE HTML>
<html>
<head>
<title>Admin - Jadwal Guru</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Admin Panel - Jadwal Guru" />
<script type="applicat ion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{ url('template/css/style.css')}}" rel='stylesheet' type='text/css' />
<link href="{{ url('template/css/font-awesome.css')}}" rel="stylesheet"> 
<link rel="stylesheet" href="{{ url('template/css/icon-font.min.css')}}" type='text/css' />
<script src="{{ url('template/js/Chart.js')}}"></script>
<link href="{{ url('template/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />
<!--animate-->
<link href="{{ url('template/css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
<link href="{{ url('template/css/bootstrap.3.2.0.min.css')}}" rel="stylesheet">


</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
        <div class="left-side sticky-left-side">

            <!--logo and iconic logo start-->
            <div class="logo">
                <h2><a href="{{url('/admin')}}">
                <b>Admin</b>Guru
                </a></h2>
            </div>
            <div class="logo-icon text-center">
                <a href="{{url('/admin')}}"><i class="fa fa-home"></i> </a>
            </div>

            <!--logo and iconic logo end-->
            <div class="left-side-inner">

                <!--sidebar nav start-->
                    <ul class="nav nav-pills nav-stacked custom-nav">
                        <li class=""><a href="{{url('/admin')}}"><i class="fa fa-power-off"></i><span>Dashboard</span></a></li>
                           <li class="menu-list">
                               <a href="#"><i class="fa fa-calendar"></i>
                                   <span>Jadwal</span></a>
                                   <ul class="sub-menu-list">
                                       <li><a href="{{url('/admin/lihatjadwal')}}">Lihat Jadwal</a> </li>
                                       <li><a href="{{url('/admin/aturjadwal')}}">Atur Jadwal</a></li>
                                   </ul>
                           </li>
                           <li class="menu-list">
                                <a href="#"><i class="fa fa-user"></i>
                                <span>Guru</span></a>
                                <ul class="sub-menu-list">
                                   <li><a href="{{url('/admin/daftarguru')}}">Daftar Guru</a> </li>
                               </ul>
                           </li>
                            <li class="menu-list">
                              <a href="#"><i class="fa fa-tasks"></i>
                              <span>Mapel</span></a>
                                <ul class="sub-menu-list">
                                   <li><a href="{{url('/admin/daftarmapel')}}">Daftar Mapel</a> </li>
                               </ul>
                           </li>
                            <li class="menu-list">
                              <a href="#"><i class="fa fa-bell"></i>
                              <span>Ruang Kelas</span></a>
                                <ul class="sub-menu-list">
                                   <li><a href="{{url('/admin/daftarruang')}}">Daftar Ruang</a> </li>
                               </ul>
                           </li>
                    </ul>
                <!--sidebar nav end-->
            </div>
        </div>
        <!-- left side end-->
    
        <!-- main content start-->
        <div class="main-content">
            <!-- header-starts -->
            <div class="header-section">
             
            <!--toggle button start-->
            <a class="toggle-btn  menu-collapsed"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            <!--notification menu start -->
            <div class="menu-right">
                <div class="user-panel-top">    
                    <div class="profile_details_left">
                        <ul class="nofitications-dropdown">
                      
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-question-circle" style="font-size:25px;margin-left:10px;"></i></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3>Panduan Jadwal Guru</h3>
                                            </div>
                                        </li>
                                        <li class="odd"><a href="{{url('/admin/panduan/aturjadwal')}}">
                                            <div class="user_img"><i class="fa fa-question"></i></div>
                                           <div class="notification_desc">
                                            <p>Cara mengatur jadwal guru?</p>
                                            </div>
                                          <div class="clearfix"></div>  
                                         </a></li>
                                         <li class="odd"><a href="{{url('/admin/panduan/lihatjadwal')}}">
                                            <div class="user_img"><i class="fa fa-question"></i></div>
                                           <div class="notification_desc">
                                            <p>Melihat Jadwal?</p>
                                            </div>
                                           <div class="clearfix"></div> 
                                         </a></li>
                                         <li class="odd"><a href="{{url('/admin/panduan/guru')}}">
                                            <div class="user_img"><i class="fa fa-question"></i></div>
                                           <div class="notification_desc">
                                            <p>Cara menambah dan mengedit guru?</p>
                                            </div>
                                           <div class="clearfix"></div> 
                                         </a></li>
                                         <li class="odd"><a href="{{url('/admin/panduan/mapel')}}">
                                            <div class="user_img"><i class="fa fa-question"></i></div>
                                           <div class="notification_desc">
                                            <p>Cara menambah dan mengedit mapel?</p>
                                            </div>
                                           <div class="clearfix"></div> 
                                         </a></li>
                                         <li class="odd"><a href="{{url('/admin/panduan/ruang')}}">
                                            <div class="user_img"><i class="fa fa-question"></i></div>
                                           <div class="notification_desc">
                                            <p>Cara menambah dan mengedit ruang kelas?</p>
                                            </div>
                                           <div class="clearfix"></div> 
                                         </a></li>
                                    </ul>
                            </li>                                               
                            <div class="clearfix"></div>    
                        </ul>
                    </div>
                    <div class="profile_details">       
                        <ul>
                            <li class="dropdown profile_details_drop">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile_img">
                                    @if(Auth::guard('admin')->user()->foto == 0)
                                        <span style="background:url({{ url('template/images/serr.png')}}) no-repeat center;width:50px;height:50px;"> </span> 
                                    @else
                                        <span style="background:url({{url(Auth::guard('admin')->user()->foto)}}) no-repeat center;width:10%;height:10%;"> </span>
                                    @endif
                                         <div class="user-name">
                                            <p>{{Auth::guard('admin')->user()->name}}<span>Admin</span></p>
                                         </div>
                                         <!-- <i class="lnr lnr-chevron-down"></i> -->
                                         <!-- <i class="lnr lnr-chevron-up"></i> -->
                                        <div class="clearfix"></div>    
                                    </div>  
                                </a>
                                <ul class="dropdown-menu drp-mnu">
                                    <li> <a href="{{url('/admin/profile')}}"><i class="fa fa-user"></i>Profile</a> </li> 
                                    <li> <a href="{{url('/admin/logout')}}"><i class="fa fa-sign-out"></i> Logout</a> </li>
                                </ul>
                            </li>
                            <div class="clearfix"> </div>
                        </ul>
                    </div>                      
                    <div class="clearfix"></div>
                </div>
              </div>
            <!--notification menu end -->
            </div>
        <!-- //header-ends -->
@yield('content')
