@extends('layouts.templateuser')
@section('content')
<div id="page-wrapper">
				<div class="graphs">
				<div class="col-md-12">
				<div class="page-wrapper">
				<div class="panel panel-info">
				<div class="panel-heading">Jadwal Hari Ini</div>
  				<div class="panel-body">
  								<center>
					<div class="form">
					@if(count($tampil) === 0)
						<div class="thumbnail">
  						<h1 style="font-family:Haettenschweiler;font-size:50px;">Oopss!</h1>
  							<img src="{{url('template/images/close.png')}}" style="width:30%;"/>
  						<h2><b>Tidak ada jadwal untuk hari ini!</b></h2>
  						</div>
				  	@else
					<center><h1 style="font-family:Haettenschweiler;font-size:50px;">Jadwal Hari Ini</h1></center>
					<br>
					<form>
				  		<table border="1" class="table" style="width:50%">
				  		<thead>
				  			<tr>
					  			<th align="center">Jam ke</th>
					  			<th align="center">Mata Pelajaran</th>
					  			<th align="center">Kelas</th>
					  			<th align="center">Ruang</th>
				  			</tr>
				  		</thead>
				  		<tbody>
					  		<tr>
					  		@if(count($tampil1) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil1 as $tampil1)
					  			<td width="10%" align="center">{{$tampil1->jamke}}.</td>
					  			<td align="center">{{$tampil1->namamapel}}</td>
					  			<td align="center">{{$tampil1->namakelas}}</td>
					  			<td align="center">{{$tampil1->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil2) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil2 as $tampil2)
					  			<td width="10%" align="center">{{$tampil2->jamke}}.</td>
					  			<td align="center">{{$tampil2->namamapel}}</td>
					  			<td align="center">{{$tampil2->namakelas}}</td>
					  			<td align="center">{{$tampil2->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil3) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil3 as $tampil3)
					  			<td width="10%" align="center">{{$tampil3->jamke}}.</td>
					  			<td align="center">{{$tampil3->namamapel}}</td>
					  			<td align="center">{{$tampil3->namakelas}}</td>
					  			<td align="center">{{$tampil3->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil4) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil4 as $tampil4)
					  			<td width="10%" align="center">{{$tampil4->jamke}}.</td>
					  			<td align="center">{{$tampil4->namamapel}}</td>
					  			<td align="center">{{$tampil4->namakelas}}</td>
					  			<td align="center">{{$tampil4->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil5) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil5 as $tampil5)
					  			<td width="10%" align="center">{{$tampil5->jamke}}.</td>
					  			<td align="center">{{$tampil5->namamapel}}</td>
					  			<td align="center">{{$tampil5->namakelas}}</td>
					  			<td align="center">{{$tampil5->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil6) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil6 as $tampil6)
					  			<td width="10%" align="center">{{$tampil6->jamke}}.</td>
					  			<td align="center">{{$tampil6->namamapel}}</td>
					  			<td align="center">{{$tampil6->namakelas}}</td>
					  			<td align="center">{{$tampil6->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil7) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil7 as $tampil7)
					  			<td width="10%" align="center">{{$tampil7->jamke}}.</td>
					  			<td align="center">{{$tampil7->namamapel}}</td>
					  			<td align="center">{{$tampil7->namakelas}}</td>
					  			<td align="center">{{$tampil7->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil8) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil8 as $tampil8)
					  			<td width="10%" align="center">{{$tampil8->jamke}}.</td>
					  			<td align="center">{{$tampil8->namamapel}}</td>
					  			<td align="center">{{$tampil8->namakelas}}</td>
					  			<td align="center">{{$tampil8->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil9) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil9 as $tampil9)
					  			<td width="10%" align="center">{{$tampil9->jamke}}.</td>
					  			<td align="center">{{$tampil9->namamapel}}</td>
					  			<td align="center">{{$tampil9->namakelas}}</td>
					  			<td align="center">{{$tampil9->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
					  		<tr>
					  		@if(count($tampil10) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil10 as $tampil10)
					  			<td width="10%" align="center">{{$tampil10->jamke}}.</td>
					  			<td align="center">{{$tampil10->namamapel}}</td>
					  			<td align="center">{{$tampil10->namakelas}}</td>
					  			<td align="center">{{$tampil10->namaruang}}</td>
					  			@endforeach
					  		@endif
					  		</tr>
				  		</tbody>	
				  		</table>
				  	</form>
				  	@endif
					</div>
			</center>

				</div>
				</div>
				</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection