@extends('layouts.templateuser')
@section('content')
<div id="page-wrapper">
<div class="graphs">
<div class="col-md-12">
<div class="page-wrapper">
<div class="panel panel-info">
<div class="panel-heading">Jadwal Per Hari</div>
  <div class="panel-body">
  <div class="form">
	@if(count($tampil) === 0)
			<center>
			<div class="thumbnail">
  			<h1 style="font-family:Haettenschweiler;font-size:50px;">Oopss!</h1>
  				<img src="{{url('template/images/close.png')}}" style="width:30%;"/>
  			<h2><b>Tidak ada jadwal untuk hari ini!</b></h2>
  			</div>
  			</center>
	@else
			<center><h1 style="font-family:Haettenschweiler;font-size:50px;">Jadwal Per Hari</h1></center>
			<br>
			<br>
			<div class="row">

			<!-- Senin -->
  			<div class="col-md-4">
  				<center><h3>Senin</h3></center>
  				<form>
  					<table border="1" class="table">
     				 <thead>
     				   <tr>
     				     <th><center>Jam Ke</center></th>
     				     <th>Mata Pelajaran</th>
    				     <th>Kelas</th>
    				     <th>Ruang</th>
     				   </tr>
     				 </thead>
     				 <tbody>
     				 	<tr>
     				 		@if(count($tampil11) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil11 as $tampil11)
					  			<td width="10%" align="center">{{$tampil11->jamke}}.</td>
					  			<td align="center">{{$tampil11->namamapel}}</td>
					  			<td align="center">{{$tampil11->namakelas}}</td>
					  			<td align="center">{{$tampil11->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil12) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil12 as $tampil12)
					  			<td width="10%" align="center">{{$tampil12->jamke}}.</td>
					  			<td align="center">{{$tampil12->namamapel}}</td>
					  			<td align="center">{{$tampil12->namakelas}}</td>
					  			<td align="center">{{$tampil12->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil13) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil13 as $tampil13)
					  			<td width="10%" align="center">{{$tampil13->jamke}}.</td>
					  			<td align="center">{{$tampil13->namamapel}}</td>
					  			<td align="center">{{$tampil13->namakelas}}</td>
					  			<td align="center">{{$tampil13->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil14) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil14 as $tampil14)
					  			<td width="10%" align="center">{{$tampil14->jamke}}.</td>
					  			<td align="center">{{$tampil14->namamapel}}</td>
					  			<td align="center">{{$tampil14->namakelas}}</td>
					  			<td align="center">{{$tampil14->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil15) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil15 as $tampil15)
					  			<td width="10%" align="center">{{$tampil15->jamke}}.</td>
					  			<td align="center">{{$tampil15->namamapel}}</td>
					  			<td align="center">{{$tampil15->namakelas}}</td>
					  			<td align="center">{{$tampil15->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil16) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil16 as $tampil16)
					  			<td width="10%" align="center">{{$tampil16->jamke}}.</td>
					  			<td align="center">{{$tampil16->namamapel}}</td>
					  			<td align="center">{{$tampil16->namakelas}}</td>
					  			<td align="center">{{$tampil16->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil17) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil17 as $tampil17)
					  			<td width="10%" align="center">{{$tampil17->jamke}}.</td>
					  			<td align="center">{{$tampil17->namamapel}}</td>
					  			<td align="center">{{$tampil17->namakelas}}</td>
					  			<td align="center">{{$tampil17->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil18) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil18 as $tampil18)
					  			<td width="10%" align="center">{{$tampil18->jamke}}.</td>
					  			<td align="center">{{$tampil18->namamapel}}</td>
					  			<td align="center">{{$tampil18->namakelas}}</td>
					  			<td align="center">{{$tampil18->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil19) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil19 as $tampil19)
					  			<td width="10%" align="center">{{$tampil19->jamke}}.</td>
					  			<td align="center">{{$tampil19->namamapel}}</td>
					  			<td align="center">{{$tampil19->namakelas}}</td>
					  			<td align="center">{{$tampil19->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil20) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil20 as $tampil20)
					  			<td width="10%" align="center">{{$tampil20->jamke}}.</td>
					  			<td align="center">{{$tampil20->namamapel}}</td>
					  			<td align="center">{{$tampil20->namakelas}}</td>
					  			<td align="center">{{$tampil20->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>     				 	      				 	
    				 </tbody>
    				 </table>
  				</form>
  			</div>

  			<!-- Selasa -->
  			<div class="col-md-4">
  				<center><h3>Selasa</h3></center>
  				<form>
  					<table border="1" class="table">
     				 <thead>
     				   <tr>
     				     <th><center>Jam Ke</center></th>
     				     <th>Mata Pelajaran</th>
    				     <th>Kelas</th>
    				     <th>Ruang</th>
     				   </tr>
     				 </thead>
     				 <tbody>
     				 	<tr>
     				 		@if(count($tampil21) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil21 as $tampil21)
					  			<td width="10%" align="center">{{$tampil21->jamke}}.</td>
					  			<td align="center">{{$tampil21->namamapel}}</td>
					  			<td align="center">{{$tampil21->namakelas}}</td>
					  			<td align="center">{{$tampil21->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil22) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil22 as $tampil22)
					  			<td width="10%" align="center">{{$tampil22->jamke}}.</td>
					  			<td align="center">{{$tampil22->namamapel}}</td>
					  			<td align="center">{{$tampil22->namakelas}}</td>
					  			<td align="center">{{$tampil22->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil23) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil23 as $tampil23)
					  			<td width="10%" align="center">{{$tampil23->jamke}}.</td>
					  			<td align="center">{{$tampil23->namamapel}}</td>
					  			<td align="center">{{$tampil23->namakelas}}</td>
					  			<td align="center">{{$tampil23->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil24) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil24 as $tampil24)
					  			<td width="10%" align="center">{{$tampil24->jamke}}.</td>
					  			<td align="center">{{$tampil24->namamapel}}</td>
					  			<td align="center">{{$tampil24->namakelas}}</td>
					  			<td align="center">{{$tampil24->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil25) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil25 as $tampil25)
					  			<td width="10%" align="center">{{$tampil25->jamke}}.</td>
					  			<td align="center">{{$tampil25->namamapel}}</td>
					  			<td align="center">{{$tampil25->namakelas}}</td>
					  			<td align="center">{{$tampil25->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil26) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil26 as $tampil26)
					  			<td width="10%" align="center">{{$tampil26->jamke}}.</td>
					  			<td align="center">{{$tampil26->namamapel}}</td>
					  			<td align="center">{{$tampil26->namakelas}}</td>
					  			<td align="center">{{$tampil26->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil27) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil27 as $tampil27)
					  			<td width="10%" align="center">{{$tampil27->jamke}}.</td>
					  			<td align="center">{{$tampil27->namamapel}}</td>
					  			<td align="center">{{$tampil27->namakelas}}</td>
					  			<td align="center">{{$tampil27->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil28) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil28 as $tampil28)
					  			<td width="10%" align="center">{{$tampil28->jamke}}.</td>
					  			<td align="center">{{$tampil28->namamapel}}</td>
					  			<td align="center">{{$tampil28->namakelas}}</td>
					  			<td align="center">{{$tampil28->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil29) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil29 as $tampil29)
					  			<td width="10%" align="center">{{$tampil29->jamke}}.</td>
					  			<td align="center">{{$tampil29->namamapel}}</td>
					  			<td align="center">{{$tampil29->namakelas}}</td>
					  			<td align="center">{{$tampil29->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil30) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil30 as $tampil30)
					  			<td width="10%" align="center">{{$tampil30->jamke}}.</td>
					  			<td align="center">{{$tampil30->namamapel}}</td>
					  			<td align="center">{{$tampil30->namakelas}}</td>
					  			<td align="center">{{$tampil30->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>     				 	      				 	
    				 </tbody>
    				 </table>
  				</form>
  			</div>

  			<!-- Rabu -->
  			<div class="col-md-4">
  				<center><h3>Rabu</h3></center>
  				<form>
  					<table border="1" class="table">
     				 <thead>
     				   <tr>
     				     <th><center>Jam Ke</center></th>
     				     <th>Mata Pelajaran</th>
    				     <th>Kelas</th>
    				     <th>Ruang</th>
     				   </tr>
     				 </thead>
     				 <tbody>
     				 	<tr>
     				 		@if(count($tampil31) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil31 as $tampil31)
					  			<td width="10%" align="center">{{$tampil31->jamke}}.</td>
					  			<td align="center">{{$tampil31->namamapel}}</td>
					  			<td align="center">{{$tampil31->namakelas}}</td>
					  			<td align="center">{{$tampil31->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil32) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil32 as $tampil32)
					  			<td width="10%" align="center">{{$tampil32->jamke}}.</td>
					  			<td align="center">{{$tampil32->namamapel}}</td>
					  			<td align="center">{{$tampil32->namakelas}}</td>
					  			<td align="center">{{$tampil32->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil33) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil33 as $tampil33)
					  			<td width="10%" align="center">{{$tampil33->jamke}}.</td>
					  			<td align="center">{{$tampil33->namamapel}}</td>
					  			<td align="center">{{$tampil33->namakelas}}</td>
					  			<td align="center">{{$tampil33->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil34) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil34 as $tampil34)
					  			<td width="10%" align="center">{{$tampil34->jamke}}.</td>
					  			<td align="center">{{$tampil34->namamapel}}</td>
					  			<td align="center">{{$tampil34->namakelas}}</td>
					  			<td align="center">{{$tampil34->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil35) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil35 as $tampil35)
					  			<td width="10%" align="center">{{$tampil35->jamke}}.</td>
					  			<td align="center">{{$tampil35->namamapel}}</td>
					  			<td align="center">{{$tampil35->namakelas}}</td>
					  			<td align="center">{{$tampil35->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil36) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil36 as $tampil36)
					  			<td width="10%" align="center">{{$tampil36->jamke}}.</td>
					  			<td align="center">{{$tampil36->namamapel}}</td>
					  			<td align="center">{{$tampil36->namakelas}}</td>
					  			<td align="center">{{$tampil36->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil37) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil37 as $tampil37)
					  			<td width="10%" align="center">{{$tampil37->jamke}}.</td>
					  			<td align="center">{{$tampil37->namamapel}}</td>
					  			<td align="center">{{$tampil37->namakelas}}</td>
					  			<td align="center">{{$tampil37->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil38) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil38 as $tampil38)
					  			<td width="10%" align="center">{{$tampil38->jamke}}.</td>
					  			<td align="center">{{$tampil38->namamapel}}</td>
					  			<td align="center">{{$tampil38->namakelas}}</td>
					  			<td align="center">{{$tampil38->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil39) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil39 as $tampil39)
					  			<td width="10%" align="center">{{$tampil39->jamke}}.</td>
					  			<td align="center">{{$tampil39->namamapel}}</td>
					  			<td align="center">{{$tampil39->namakelas}}</td>
					  			<td align="center">{{$tampil39->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil40) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil40 as $tampil40)
					  			<td width="10%" align="center">{{$tampil40->jamke}}.</td>
					  			<td align="center">{{$tampil40->namamapel}}</td>
					  			<td align="center">{{$tampil40->namakelas}}</td>
					  			<td align="center">{{$tampil40->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>     				 	      				 	
    				 </tbody>
    				 </table>
  				</form>
  			</div>

  			<div class="col-md-2"></div>
  			<!-- Kamis -->
  			<div class="col-md-4">
  				<center><h3>Kamis</h3></center>
  				<form>
  					<table border="1" class="table">
     				 <thead>
     				   <tr>
     				     <th><center>Jam Ke</center></th>
     				     <th>Mata Pelajaran</th>
    				     <th>Kelas</th>
    				     <th>Ruang</th>
     				   </tr>
     				 </thead>
     				 <tbody>
     				 	<tr>
     				 		@if(count($tampil41) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil41 as $tampil41)
					  			<td width="10%" align="center">{{$tampil41->jamke}}.</td>
					  			<td align="center">{{$tampil41->namamapel}}</td>
					  			<td align="center">{{$tampil41->namakelas}}</td>
					  			<td align="center">{{$tampil41->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil42) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil42 as $tampil42)
					  			<td width="10%" align="center">{{$tampil42->jamke}}.</td>
					  			<td align="center">{{$tampil42->namamapel}}</td>
					  			<td align="center">{{$tampil42->namakelas}}</td>
					  			<td align="center">{{$tampil42->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil43) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil43 as $tampil43)
					  			<td width="10%" align="center">{{$tampil43->jamke}}.</td>
					  			<td align="center">{{$tampil43->namamapel}}</td>
					  			<td align="center">{{$tampil43->namakelas}}</td>
					  			<td align="center">{{$tampil43->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil44) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil44 as $tampil44)
					  			<td width="10%" align="center">{{$tampil44->jamke}}.</td>
					  			<td align="center">{{$tampil44->namamapel}}</td>
					  			<td align="center">{{$tampil44->namakelas}}</td>
					  			<td align="center">{{$tampil44->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil45) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil45 as $tampil45)
					  			<td width="10%" align="center">{{$tampil45->jamke}}.</td>
					  			<td align="center">{{$tampil45->namamapel}}</td>
					  			<td align="center">{{$tampil45->namakelas}}</td>
					  			<td align="center">{{$tampil45->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil46) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil46 as $tampil46)
					  			<td width="10%" align="center">{{$tampil46->jamke}}.</td>
					  			<td align="center">{{$tampil46->namamapel}}</td>
					  			<td align="center">{{$tampil46->namakelas}}</td>
					  			<td align="center">{{$tampil46->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil47) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil47 as $tampil47)
					  			<td width="10%" align="center">{{$tampil47->jamke}}.</td>
					  			<td align="center">{{$tampil47->namamapel}}</td>
					  			<td align="center">{{$tampil47->namakelas}}</td>
					  			<td align="center">{{$tampil47->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil48) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil48 as $tampil48)
					  			<td width="10%" align="center">{{$tampil48->jamke}}.</td>
					  			<td align="center">{{$tampil48->namamapel}}</td>
					  			<td align="center">{{$tampil48->namakelas}}</td>
					  			<td align="center">{{$tampil48->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil49) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil49 as $tampil49)
					  			<td width="10%" align="center">{{$tampil49->jamke}}.</td>
					  			<td align="center">{{$tampil49->namamapel}}</td>
					  			<td align="center">{{$tampil49->namakelas}}</td>
					  			<td align="center">{{$tampil49->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil50) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil50 as $tampil50)
					  			<td width="10%" align="center">{{$tampil50->jamke}}.</td>
					  			<td align="center">{{$tampil50->namamapel}}</td>
					  			<td align="center">{{$tampil49->namakelas}}</td>
					  			<td align="center">{{$tampil50->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>     				 	      				 	
    				 </tbody>
    				 </table>
  				</form>
  			</div>

  			<!-- Jumat -->
  			<div class="col-md-4">
  				<center><h3>Jumat</h3></center>
  				<form>
  					<table border="1" class="table">
     				 <thead>
     				   <tr>
     				     <th><center>Jam Ke</center></th>
     				     <th>Mata Pelajaran</th>
    				     <th>Kelas</th>
    				     <th>Ruang</th>
     				   </tr>
     				 </thead>
     				 <tbody>
     				 	<tr>
     				 		@if(count($tampil51) === 0)
					  			<td width="10%" align="center">1.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil51 as $tampil51)
					  			<td width="10%" align="center">{{$tampil51->jamke}}.</td>
					  			<td align="center">{{$tampil51->namamapel}}</td>
					  			<td align="center">{{$tampil51->namakelas}}</td>
					  			<td align="center">{{$tampil51->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil52) === 0)
					  			<td width="10%" align="center">2.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil52 as $tampil52)
					  			<td width="10%" align="center">{{$tampil52->jamke}}.</td>
					  			<td align="center">{{$tampil52->namamapel}}</td>
					  			<td align="center">{{$tampil52->namakelas}}</td>
					  			<td align="center">{{$tampil52->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil53) === 0)
					  			<td width="10%" align="center">3.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil53 as $tampil53)
					  			<td width="10%" align="center">{{$tampil53->jamke}}.</td>
					  			<td align="center">{{$tampil53->namamapel}}</td>
					  			<td align="center">{{$tampil53->namakelas}}</td>
					  			<td align="center">{{$tampil53->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil54) === 0)
					  			<td width="10%" align="center">4.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil54 as $tampil54)
					  			<td width="10%" align="center">{{$tampil54->jamke}}.</td>
					  			<td align="center">{{$tampil54->namamapel}}</td>
					  			<td align="center">{{$tampil54->namakelas}}</td>
					  			<td align="center">{{$tampil54->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil55) === 0)
					  			<td width="10%" align="center">5.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil55 as $tampil55)
					  			<td width="10%" align="center">{{$tampil55->jamke}}.</td>
					  			<td align="center">{{$tampil55->namamapel}}</td>
					  			<td align="center">{{$tampil55->namakelas}}</td>
					  			<td align="center">{{$tampil55->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil56) === 0)
					  			<td width="10%" align="center">6.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil56 as $tampil56)
					  			<td width="10%" align="center">{{$tampil56->jamke}}.</td>
					  			<td align="center">{{$tampil56->namamapel}}</td>
					  			<td align="center">{{$tampil56->namakelas}}</td>
					  			<td align="center">{{$tampil56->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil57) === 0)
					  			<td width="10%" align="center">7.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil57 as $tampil57)
					  			<td width="10%" align="center">{{$tampil57->jamke}}.</td>
					  			<td align="center">{{$tampil57->namamapel}}</td>
					  			<td align="center">{{$tampil57->namakelas}}</td>
					  			<td align="center">{{$tampil57->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil58) === 0)
					  			<td width="10%" align="center">8.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil58 as $tampil58)
					  			<td width="10%" align="center">{{$tampil58->jamke}}.</td>
					  			<td align="center">{{$tampil58->namamapel}}</td>
					  			<td align="center">{{$tampil58->namakelas}}</td>
					  			<td align="center">{{$tampil58->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil59) === 0)
					  			<td width="10%" align="center">9.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil59 as $tampil59)
					  			<td width="10%" align="center">{{$tampil59->jamke}}.</td>
					  			<td align="center">{{$tampil59->namamapel}}</td>
					  			<td align="center">{{$tampil59->namakelas}}</td>
					  			<td align="center">{{$tampil59->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>
     				 	<tr>
     				 		@if(count($tampil60) === 0)
					  			<td width="10%" align="center">10.</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  			<td align="center">-</td>
					  		@else
					  			@foreach($tampil60 as $tampil60)
					  			<td width="10%" align="center">{{$tampil60->jamke}}.</td>
					  			<td align="center">{{$tampil60->namamapel}}</td>
					  			<td align="center">{{$tampil60->namakelas}}</td>
					  			<td align="center">{{$tampil60->namaruang}}</td>
					  			@endforeach
					  		@endif
     				 	</tr>     				 	      				 	
    				 </tbody>
    				 </table>
  				</form>
  				@endif
  			</div>
  			<div class="col-md-2"></div>
  			</div>
				</div>
				</div>
				</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection