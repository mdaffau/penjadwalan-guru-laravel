<!DOCTYPE HTML>
<html>
<head>
<title>Landing Page | Sistem Informasi Penjadwalan Guru Jurusan</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<link href="{{ url('landing/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ url('landing/css/slimmenu.css')}}" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="{{ url('landing/css/magnific-popup.css')}}">
<script src="{{ url('landing/js/jquery.min.js')}}"></script>
</head>
<body>

<div class="content" id="home">
	<div class="header">
		<div class="wrap">
		      <header id="topnav">
			        <nav>
			        		 <ul>
			        		 	<li class="active"><a href="#home" class="scroll">Home</a></li>
			        		 	<li><a href="#team" class="scroll">Fitur</a></li>
								<li><a href="#contact" class="scroll">Saran dan Keluhan</a></li>											
								<div class="clear"> </div>
							</ul>
		        	</nav>
			         <h1><a href="{{url('index')}}"><b>Jadwal</b>Guru</a></h1>

			        	<a href="#" id="navbtn">Nav Menu</a>

			        <div class="clear"> </div>
		        </header>
		    </div>
		</div>
		<!-----script------------->
		<script type="text/javascript"  src="{{ url('landing/js/menu.js')}}"></script>

	<div class="slider" id="home"> 	
		<div class="wrap">
				<!---start-da-slider----->
			<div id="da-slider" class="da-slider">
				<div class="da-slide">
					<h2>Sistem Informasi Penjadwalan Guru Jurusan</h2>
					<p>Sebuah sistem informasi yang digunakan untuk menangani 
					kebutuhan tentang sistem penjadwalan guru di salah satu jurusan</p>
					<a href="{{url('/index')}}" class="btn da-link">Login Disini</a>
				</div>
				<div class="da-slide">
					<h2>Sistem Informasi Penjadwalan Guru Jurusan</h2>
					<p>Sebuah sistem informasi yang digunakan untuk menangani 
					kebutuhan tentang sistem penjadwalan guru di salah satu jurusan</p>
					<a href="{{url('/index')}}" class="btn da-link">Login Disini</a>
				</div>
				<div class="da-slide">
					<h2>Sistem Informasi Penjadwalan Guru Jurusan</h2>
					<p>Sebuah sistem informasi yang digunakan untuk menangani 
					kebutuhan tentang sistem penjadwalan guru di salah satu jurusan</p>
					<a href="{{url('/index')}}" class="btn da-link">Login Disini</a>
				</div>
				<div class="da-slide">
					<h2>Sistem Informasi Penjadwalan Guru Jurusan</h2>
					<p>Sebuah sistem informasi yang digunakan untuk menangani 
					kebutuhan tentang sistem penjadwalan guru di salah satu jurusan</p>
					<a href="{{url('/index')}}" class="btn da-link">Login Disini</a>
				</div>
			</div>
		
			<script type="text/javascript" src="{{ url('landing/js/jquery.cslider.js')}}"></script>
					 <!---strat-slider---->
				    <link rel="stylesheet" type="text/css" href="{{ url('landing/css/slider.css')}}" />
					<script type="text/javascript" src="{{ url('landing/js/modernizr.custom.28468.js')}}"></script>
						<script type="text/javascript">
							$(function() {
							
								$('#da-slider').cslider({
									autoplay	: true,
									bgincrement	: 450
								});
							
							});
					</script>
					<!---//End-da-slider----->
			
		</div>
	</div>
</div>
<!---start-team----->
	<div  class="team" id="team">
		<div class="wrap">
			<div class="section group">
				<div class="col_1_of_1 span_1_of_1">
				   <h3>Keunggulan Sistem Informasi Penjadwalan Guru Jurusan</h3>
				  <p>Keunggulan sistem informasi ini terdapat pada sistem pengaturan jadwal guru nya.
				  Serta dilengkapi multi login untuk admin dan guru, selain admin dapat mengatur jadwal,
				  guru juga dapat melihat jadwal yang mereka miliki. Dilengkapi juga dengan user guide 
				  pada setiap halaman.</p>
				</div>
				<div class="pen">
					<img src="{{url('template/images/info.png')}}" style="width:10%;">
				</div>
		 </div>
		</div>
	</div>

			<!-- Add fancyBox light-box -->
				<script src="{{ url('landing/js/jquery.magnific-popup.js')}}" type="text/javascript"></script>

				<script>
					$(document).ready(function() {
						$('.popup-with-zoom-anim').magnificPopup({
							type: 'inline',
							fixedContentPos: false,
							fixedBgPos: true,
							overflowY: 'auto',
							closeBtnInside: true,
							preloader: false,
							midClick: true,
							removalDelay: 300,
							mainClass: 'my-mfp-zoom-in'
					});
				});
		</script>
		<!----End-pricingplans---->
		<!----start-contact---->
		 <div  class="contact" id="contact">
		 	<div class="contact">
		 		<h3>Saran & Keluhan</h3>
		 		<p>Masukkan saran atau keluhan anda tentang sistem informasi dibawah ini</p>
		 		<div class="wrap">
		 			<div class="con">
				  			<form method="post" enctype="multipart/form-data">
				  			{{csrf_field()}}
				  					<input type="text" placeholder="Name" name="nama" required/> 
							    	<input type="text" placeholder="Email" name="email" required/>
										<div class="clear"> </div>
								    <div>
								    	<textarea placeholder="Message" name="pesan">Message</textarea>
								    </div>
									 <div class="con-button">
										<input type="submit" value="Send" name="simpan">
									</div>
							</form>
						</div>
						 <div class="clear"> </div>
					 <div class="social_icon">	
					           <ul>	
								    
								    <li class="twitter"><a href="#"><span> </span></a></li>
								    <li class="facebook"><a href="#"><span> </span></a></li>	 	
									<li class="google"><a href="#"><span> </span></a></li>
							  </ul>
		 	  		</div>
				</div>
		 	</div>
		 </div>
		<!----//End-contact---->
				<div class="footer">
					<div class="wrap">
						 <div class="footer-con">

								<div class="footer-right">
					        		 <ul>
					        		 	<li class="active"><a href="#home" class="scroll">Home</a></li>
			        		 			<li><a href="#team" class="scroll">Fitur</a></li>
										<li><a href="#contact" class="scroll">Saran dan Keluhan</a></li>
										<div class="clear"> </div>
									</ul>
								</div>
									<div class="footer-left">
										<p> 2014 &#169; Template by <a href="http://www.w3schools.com/">W3layouts</a> | ReDesign by <b>Dafuq</b></p>

									</div>								
								<div class="clear"> </div>
						</div>
						
					</div>
				
				</div>
			  <!-- scroll_top_btn -->
					<script type="text/javascript" src="{{ url('landing/js/move-top.js')}}"></script>
					<script type="text/javascript" src="{{ url('landing/js/easing.js')}}"></script>
				    <script type="text/javascript">
						$(document).ready(function() {
						
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
					
					<!---smoth-scrlling---->
				<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
		
					<a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
		  
	 </body>
</html>
