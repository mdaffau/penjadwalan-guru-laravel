@extends(Auth::guard('admin')->check() ? 'layouts.app_admin' : 'layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<head>
    <title>Login Form</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="{{url('form/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONT AWESOME CSS -->
    <link href="{{url('form/font-awesome.min.css')}}" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="{{url('form/style.css')}}" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Nova+Flat' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
</head>
<body background="white">
<section style="padding:50px 0px 0px 0px;" >
        <div class="container">
            <div class="row">
                <center><p><h1 style="font-family:Haettenschweiler;font-size:55px;padding-bottom:50px;">Halaman Login</h1></p></center>
                <div class="col-md-6">
                   <div class="alert alert-danger">
                        <div class="media">
                        <div class="pull-left">
                            <img src="{{url('form/admin.png')}}" class="img-responsive" />
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Login Admin</h3>
                            <p>
                                Ini adalah menu Login untuk Admin.
                                Klik button dibawah ini untuk menuju halaman Login Admin. 
                            </p>
                             <a href="{{url('/admin/login')}}" class="btn btn-danger">Admin Login</a>
                        </div>
                    </div> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="alert alert-info">
                        <div class="media">
                        <div class="pull-left">
                            <img src="{{url('form/admin.png')}}" class="img-responsive" />
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Login Guru</h3>
                            <p>
                                Ini adalah menu Login untuk Guru.
                                Klik button dibawah ini untuk menuju halaman Login Guru. 
                            </p>
                             <a href="{{url('/login')}}" class="btn btn-primary" >Guru Login</a>
                        </div>
                    </div>
                    </div>
                </div>

                </div>
                
            </div>

    </section>
</body>
</html>

@endsection
