@extends('layouts.templateadmin')
@section('content')
      <div id="page-wrapper">
        <div class="graphs">
<div class="col-md-12">
<div class="page-wrapper">
<div class="panel panel-info">
  <div class="panel-heading">Pilih Jadwal Harian</div>
  <div class="panel-body">
  <form>
   <div class="form-group">
   <label>
     Pilih Hari :
   </label>
  <select name="selector1" id="hari" class="form-control1">
  @foreach($hari as $dino)
                    <option value="{{$dino->idhari}}">{{$dino->namahari}}</option>
  @endforeach
  </select>
  </form>
  <div class="row">
  <center>

  </center>
  <br>
  </div>

  <div class="row">
  <div class="col-md-4">
  <center><h3>X RPL 1</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal1>
      @foreach($ambil as $njupuk)
        <tr id="jadwalke{{$njupuk->idjadwal}}">
          <td width="10%" align="center">{{$njupuk->jamke}}</td>
          <td width="50%">{{$njupuk->namaguru}}</td>
          <td width="30%">{{$njupuk->namamapel}}</td>
          <td width="30%">{{$njupuk->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk->idjadwal}})"></a></i></td>
        </tr>
      @endforeach 
      </tbody>
      </table>
    </form> 
  </div>

  <div class="col-md-4" >
  <center><h3>X RPL 2</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal2>

      @foreach($ambil1 as $njupuk1)
        <tr id="jadwalke{{$njupuk1->idjadwal}}">
          <td width="10%" align="center">{{$njupuk1->jamke}}</td>
          <td width="50%">{{$njupuk1->namaguru}}</td>
          <td width="30%">{{$njupuk1->namamapel}}</td>
          <td width="30%">{{$njupuk1->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk1->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk1->idjadwal}})"></a></i></td>
        </tr>
      @endforeach 
      </tbody>

      </table>
    </form>
  </div>

    <div class="col-md-4">
  <center><h3>XI RPL 1</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal3>
           @foreach($ambil2 as $njupuk2)
        <tr id="jadwalke{{$njupuk2->idjadwal}}">
          <td width="10%" align="center">{{$njupuk2->jamke}}</td>
          <td width="50%">{{$njupuk2->namaguru}}</td>
          <td width="30%">{{$njupuk2->namamapel}}</td>
          <td width="30%">{{$njupuk2->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk2->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk2->idjadwal}})"></a></i></td>
        </tr>
                    @endforeach 
      </tbody>
      </table>
    </form>
  </div>
</div>
  
<div class="row"></div>
    <div class="col-md-4" style="margin-top:50px;">
  <center><h3>XI RPL 2</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal4>
            @foreach($ambil3 as $njupuk3)
        <tr id="jadwalke{{$njupuk3->idjadwal}}">
          <td width="10%" align="center">{{$njupuk3->jamke}}</td>
          <td width="50%">{{$njupuk3->namaguru}}</td>
          <td width="30%">{{$njupuk3->namamapel}}</td>
          <td width="30%">{{$njupuk3->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk3->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk3->idjadwal}})"></a></i></td>
        </tr>
              @endforeach 
      </tbody>
      </table>
    </form>
  </div>

    <div class="col-md-4" style="margin-top:50px;">
  <center><h3>XII RPL 1</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal5>
              @foreach($ambil4 as $njupuk4)
        <tr id="jadwalke{{$njupuk4->idjadwal}}">
          <td width="10%" align="center">{{$njupuk4->jamke}}</td>
          <td width="50%">{{$njupuk4->namaguru}}</td>
          <td width="30%">{{$njupuk4->namamapel}}</td>
          <td width="30%">{{$njupuk4->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk4->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk4->idjadwal}})"></a></i></td>
        </tr>
              @endforeach 
      </tbody>
      </table>
    </form>
  </div>

    <div class="col-md-4" style="margin-top:50px;">
  <center><h3>XII RPL 2</h3></center>
    <form>
      <table border="1" class="table">
      <thead>
        <tr>
          <th><center>Jam</center></th>
          <th>Nama Guru</th>
          <th>Mapel</th>
          <th>Ruang</th>
          <th></th>
        </tr>
      </thead>
      <tbody id=jadwal6>
            @foreach($ambil5 as $njupuk5)
        <tr id="jadwalke{{$njupuk5->idjadwal}}">
          <td width="10%" align="center">{{$njupuk5->jamke}}</td>
          <td width="50%">{{$njupuk5->namaguru}}</td>
          <td width="30%">{{$njupuk5->namamapel}}</td>
          <td width="30%">{{$njupuk5->namaruang}}</td>
          <td width="10%"><i class="fa fa-pencil" value="{{$njupuk5->idjadwal}}" id="edit-jadwal" onclick="editjadwal({{$njupuk5->idjadwal}})"></a></i></td>
        </tr>
              @endforeach  
      </tbody> 
      </table>
    </form>
  </div>


  </div>


  </div>
</div>
</div>

</div>

<!-- //modal gan -->

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Jadwal</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                            <input type="hidden" id="idjadwal" name="idjadwal" value="">
                                <div class="form-group">
                                    <label for="inputTask" class="col-sm-3 control-label">Guru</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="namaguru" name="namaguru">
                                        @foreach($guru as $ngajar)
                                            <option value="{{$ngajar->idguru}}">{{$ngajar->namaguru}}</option>
                                        @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputDescription" class="col-sm-3 control-label">Mata Pelajaran</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="namamapel" name="namamapel">
                                        @foreach($mapel as $pelajaran)
                                          <option value="{{$pelajaran->idmapel}}">{{$pelajaran->namamapel}}</option>
                                        @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputDescription" class="col-sm-3 control-label">Ruang Kelas</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="namaruang" name="namaruang">
                                        @foreach($ruang as $kelas)
                                          <option value="{{$kelas->idruang}}">{{$kelas->namaruang}}</option>
                                        @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Simpan</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <meta name="_token" content="{!! csrf_token() !!}" />

        <!-- //end -->
<!-- End Bootstrap modal -->

        <div class="clearfix"></div>
      </div>
    </div>
      
    </div>
        </div>
      <!--body wrapper start-->
      </div>
       <!--body wrapper end-->
    </div>
        <!--footer section start-->
      <footer>
         <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="https://www.facebook.com/m.daffau" target="_blank">Dafuq</a></p>
      </footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
  <script>
     new WOW().init();
  </script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            var url = "/admin/aturjadwal";

            $('#hari').on('change', function(e){
                var idhari = e.target.value;     
                $.get( url + '/' + idhari, function(data){
                    console.log(idhari);
                    $('#jadwal1').empty();
                    $('#jadwal2').empty();
                    $('#jadwal3').empty();
                    $('#jadwal4').empty();
                    $('#jadwal5').empty();
                    $('#jadwal6').empty();
                    $.each(data, function(index, element){
                        dataJadwal = element;
                        $.each(dataJadwal.ambil, function(i, e){
                          $('#jadwal1').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil' value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                        $.each(dataJadwal.ambil1, function(i, e){
                          $('#jadwal2').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil'value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                        $.each(dataJadwal.ambil2, function(i, e){
                          $('#jadwal3').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil' value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                        $.each(dataJadwal.ambil3, function(i, e){
                          $('#jadwal4').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil' value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                           $.each(dataJadwal.ambil4, function(i, e){
                          $('#jadwal5').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil' value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                            $.each(dataJadwal.ambil5, function(i, e){
                          $('#jadwal6').append("<tr id='jadwalke"+e.idjadwal+"'><td width='10%' align='center'>"+e.jamke+"</td><td width='50%'>"+e.namaguru+"</td><td  width='30%'>"+e.namamapel+"</td><td  width='30%'>"+e.namaruang+"</td><td width='10%'><i class='fa fa-pencil' value='"+e.idjadwal+"' id='edit-jadwal' onclick='editjadwal("+e.idjadwal+")'></td></tr>");
                        });
                    });
                });
            });
        });
          function editjadwal($idjadwal){
              $('#frmTasks')[0].reset(); // reset form on modals
              $('.form-group').removeClass('has-error'); // clear error class
              $('.help-block').empty(); // clear error string
           
              //Ajax Load data from ajax
              $.ajax({
                  url : "/admin/aturjadwal/edit/" + $idjadwal,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {
           
                      $('#idjadwal').val(data.idjadwal);
                      $('#namaguru').val(data.idguru);
                      $('#namamapel').val(data.idmapel);
                      $('#namaruang').val(data.idruang);
                      $('#btn-save').val("update");
                      $('#myModal').modal('show'); // show bootstrap modal when complete loaded
                      $('.modal-title').text('Edit Jadwal'); // Set title to Bootstrap modal title
           
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error get data from ajax');
                  }
              });
          }
      $(document).ready(function(){
            var url = "/admin/aturjadwal";

            $("#btn-save").click(function (e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                e.preventDefault(); 
                var formData = {
                    idguru : $('#namaguru').val(),
                    idmapel : $('#namamapel').val(),
                    idruang : $('#namaruang').val(),
                }
                //used to determine the http verb to use [add=POST], [update=PUT]
                var state = $('#btn-save').val();
                var type = "POST"; //for creating new resource
                var idjadwal = $('#idjadwal').val();
                var my_url = url;
                if (state == "update"){
                    type = "PUT"; //for updating existing resource
                    my_url += '/edit/' + idjadwal;
                }
                console.log(formData);
                $.ajax({
                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                        if(data.message) {
                          alert(data.message);
                        }else {
                          var jadwal = '<tr><td width="10%" align="center">' + data.jamke + '</td><td td width="50%">' + data.guru.namaguru + '</td><td width="30%">' + data.mapel.namamapel + '</td><td width="30%">' + data.ruang.namaruang + '</td><td width="10%"><i class="fa fa-pencil" value="'+ data.idjadwal+ '" id="edit-jadwal" onclick="editjadwal('+ data.idjadwal+ ')"></i></td></tr>';
                          if (state == "add"){ //if user added a new record
                              $('#jadwal').append(jadwal);
                          }else{ //if user updated an existing record
                              $("#jadwalke" + idjadwal).replaceWith( jadwal );
                          }  
                        }
                        
                        $('#frmTasks').trigger("reset");
                        $('#myModal').modal('hide')
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
    </script>
</body>
</html>
@endsection