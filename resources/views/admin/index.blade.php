@extends('layouts.templateadmin')
@section('content')
			<div id="page-wrapper">
				<div class="graphs">
				<center>
				<div class="col-md-12">
				<div class="page-wrapper">
				<div class="panel panel-info">
					<div class="form">
					
					
  						<div class="thumbnail">
  						<h1 style="font-family:Haettenschweiler;font-size:50px;">Admin Jadwal Guru</h1>
  							<img src="{{url('logosmk1.png')}}"/>
  						<h3>Welcome, <b>{{Auth::guard('admin')->user()->name}}</b></h3>
						<a href="{{url('/admin/lihatjadwal')}}" class="btn btn-danger">Lihat Jadwal</a>
  						</div>
  						
					
					</div>
					</div>
					</div>

				<div class="clearfix"></div>
			</div>
			</center>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection