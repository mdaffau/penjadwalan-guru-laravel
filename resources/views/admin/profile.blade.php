@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
	<div class="panel panel-info">
  <div class="panel-heading">Profile</div>
  <div class="panel-body">
  @foreach($tampil as $tampil)
  <div class="col-md-4">
  <div class="image">

    @if($tampil->foto == 0)
    	<img src="{{url('template/images/profile.jpg')}}" style="width:80%; height:80%;">
    @else
      <img src="{{url('fotoadmin/'$tampil->foto)}}" style="width:80%; height:80%;">
    @endif
    	<hr>
      <a href="/admin/gantifoto/{{$tampil->id}}" class="btn btn-info">Ganti Foto</a>
  </div>
  </div>

  <div class="col-md-8">
  <div class="input-profile">
  <center><h3 style="font-family:Haettenschweiler;font-size:40px;">Profil Admin</h3></center>
  <br>
  <div class="form-group">
  <label>Nama : </label>
  <input type="text" name="nama" class="form-control" style="width:50%;" value="{{$tampil->name}}" disabled >
  <br>
  <label>Email</label>
  <input type="email" name="email" class="form-control" style="width:50%;" value="{{$tampil->email}}" disabled >
  <br>
  <a href="/admin/gantipassword/{{$tampil->id}}" class="btn btn-danger">Ganti Password</a>
  </div>
  </div>
  </div>
  @endforeach
  </div>
</div>

</div>
	</div>

				<div class="clearfix"></div>
			</div>
		</div>

		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   <script src="{{ url('template/js/wow.min.js')}}"></script>
   <script>
   new WOW().init();
   </script>
   <script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
   <script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
   <script src="{{ url('template/js/scripts.js')}}"></script>
   <!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>

</body>
</html>
@endsection