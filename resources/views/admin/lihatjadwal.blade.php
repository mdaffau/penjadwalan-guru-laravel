@extends('layouts.templateadmin')
@section('content')
			<div id="page-wrapper">
				<div class="graphs">
<div class="col-md-12">
<div class="page-wrapper">
<div class="panel panel-info">
  <div class="panel-heading">Lihat Jadwal</div>
  <div class="panel-body">
  <h5>Pilih Hari :</h5>
  <form>
   <div class="form-group">
			<select name="selector1" id="hari" class="form-control1">
			 @foreach($hari as $dino)
				<option value="{{$dino->idhari}}">{{$dino->namahari}}</option>
			@endforeach
			</select>
	</div>
	<h5>Pilih Kelas :</h5>
   <div class="form-group">
			<select name="selector1" id="kelas" class="form-control1">
				<option value=""></option>
			</select>
	</div>
  </form>


  <center>
  <br>
  	<form>
  		<table border="1" class="table" style="width:50%">
  		<thead>
  			<tr>
	  			<th>Jam ke</th>
	  			<th>Nama Guru</th>
          <th>Mata Pelajaran</th>
	  			<th>Ruang Kelas</th>
  			</tr>
  		</thead>
  		<tbody id=tabel>
  			<tr>
  				<td align="center">1.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
  			<tr>
  				<td align="center">2.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
   			<tr>
  				<td align="center">3.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">4.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">5.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">6.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">7.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">8.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">9.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
    			<tr>
  				<td align="center">10.</td>
  				<td></td>
          <td></td>
  				<td></td>
  			</tr>
  		</tbody>	
  		</table>
  	</form>
  </center>

  </div>
  </div>



  </div>
</div>
</div>

</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
   <script type="text/javascript">
        $(document).ready(function(){
            $('#hari').on('change', function(e){
                var idhari = e.target.value;     
                $.get('{{ url('/admin/lihatjadwal')}}/'+idhari, function(data){
                    console.log(idhari);
                    console.log(data);
                    $('#kelas').empty();
                    $.each(data, function(index, element){
                          $('#kelas').append("<option value="+element.idkelas+">"+element.namakelas+"</option>");
                        });
                });
            });
            $('#hari').on('change', function(eh){
            $('#kelas').on('change', function(ek){
             	var idhari = eh.target.value; 
                var idkelas = ek.target.value;     
                $.get('{{ url('/admin/lihatjadwal')}}/'+idhari+'/'+idkelas, function(data){
                    console.log(idhari);
                    console.log(idkelas);
                    console.log(data);
                    $('#tabel').empty();
                    $.each(data, function(index, element){
                          $('#tabel').append("<tr><td align='center'>"+element.jamke+".</td><td>"+element.namaguru+"</td><td>"+element.namamapel+"</td><td>"+element.namaruang+"</td></tr>");
                        });
                });
            });
        });
      });
    </script>
</body>
</html>
@endsection