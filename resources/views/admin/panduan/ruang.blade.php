@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
<div class="col-md-12">
						<div class="Compose-Message">
						<div class="panel panel-default">
						<div class="panel-heading">
							Cara mengatur data ruang kelas
						</div>
						<div class="panel-body panel-body-com-m">
							<div class="alert alert-info">
								Berikut ini panduan nya :
							</div>
							
						<form>
							<tr>
								<td>1.</td>
								<td>Pertama - tama pilih icon </td>
								<td><img src="{{url('template/images/panduan/ruang.jpg')}}"> pada sidebar </td>
							</tr>
							<br>
							<br>
							<tr>
								<td>2.</td>
								<td>Kemudian pilih</td>
								<td><img src="{{url('template/images/panduan/ruang2.jpg')}}"></td>
							</tr>
							<br>
							<br>
							<tr>
								<td>3.</td>
								<td>Maka kemudian akan muncul halaman untuk mengatur ruang kelas, di halaman ini anda dapat <b>menambah, mengedit, serta menghapus</b> Daftar Ruang Kelas</td>
							</tr>						
							</form>
					</div>
					</div>
					</div>
					</div>
</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{ url('databel/jquery.dataTables.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{ url('databel/dataTables.bootstrap.min.js')}}"></script>
<!-- App scripts -->
</body>
</html>
@endsection