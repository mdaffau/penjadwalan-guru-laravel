@extends('layouts.templatedata')
@section('content')
			<div id="page-wrapper">
				<div class="graphs">
<div class="col-md-12">
<div class="page-wrapper">
<div class="panel panel-info">
  <div class="panel-heading">Daftar Guru</div>
  <br>
  <br>
  					<center><h2>Daftar Guru</h2></center>
 						<div class="panel-body">
  						<a href="{{url('/admin/createguru')}}" class="btn btn-success">Tambah Guru</a>
  						<form>
   						<div class="form-group">
   						<br>
   						<table class="table table-bordered" id="data-guru">
							<thead>
								<tr>
									<td>No</td>
									<td>Nama Guru</td>
									<td>Jenis Kelamin</td>
									<td>Alamat</td>
									<td>Email</td>
									<td>Perintah</td>
								</tr>
							</thead>
              <tbody>
                <?php $no = 1;?>
                @foreach($dataguru as $data)
                <tr>
                  <td>{{$no++}}</td>
                  <td>{{$data->namaguru}}</td>  
                  <td>{{$data->jkguru}}</td>
                  <td>{{$data->emailguru}}</td>
                  <td>{{$data->alamatguru}}</td>
                  <td>
                    <a href="/admin/editguru/{{$data->idguru}}" class="btn btn-success">Edit</a>&nbsp;
                    <a href="/admin/deleteguru/{{$data->idguru}}" onclick="return confirm('Anda yakin mau menghapus data ini?')" class="btn btn-danger">Hapus</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
						</table>
						</div>
 						</form>
 					    </div>
  </div>



  </div>
</div>
</div>

</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
<!-- App scripts -->
<script src="{{url('datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">  
  $(function () {
    $('#data-guru').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "pagingType": "full_numbers",
    });
  });
</script>
</body>
</html>
@endsection