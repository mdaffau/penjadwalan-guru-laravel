@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
<div class="col-md-12">
						<div class="Compose-Message">
						<div class="panel panel-default">
						<div class="panel-heading">
							Ganti Password
						</div>
						<div class="panel-body panel-body-com-m">
							<div class="alert alert-info">
								Silahkan isi untuk mengganti password anda
							</div>
						<form method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{$password->id}}">
								<hr>
							<div class="form-group">
								<label class="control-label">Password Baru : </label>
								<br>
								<input type="password" name="password" class="form-control1 control3" value="{{old('password') }}">
								<span class="error"><?php echo $errors->first('password') ?></span>
								<br>
							</div>
							<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
							<input type="reset" name="reset" value="Reset" class="btn btn-danger">
							<hr>
						</form>
					</div>
					</div>
					</div>
					</div>
</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection