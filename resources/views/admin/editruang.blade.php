@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
<div class="col-md-12">
            <div class="Compose-Message">
            <div class="panel panel-default">
            <div class="panel-heading">
              Edit Ruang Kelas
            </div>
            <div class="panel-body panel-body-com-m">
              <div class="alert alert-info">
                Silahkan edit isi form edit ruang dibawah
              </div>
            <form action="{{ url('/admin/editruang', $odit->idruang) }}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="idmapel" value="{{$odit->idruang}}">
                <hr>
              <div class="form-group">
                <label class="control-label">Nama Ruang : </label>
                <br>
                <input type="text" name="namaruang" class="form-control1 control3" value="{{$odit->namaruang}}">
              </div>
              <button type="submit" class="btn btn-success">Simpan</button>
              
              <hr>
            </form>
          </div>
          </div>
          </div>
          </div>
</div>

        <div class="clearfix"></div>
      </div>
    </div>
      
    </div>
        </div>
      <!--body wrapper start-->
      </div>
       <!--body wrapper end-->
    </div>
        <!--footer section start-->
      <footer>
         <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
      </footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
  <script>
     new WOW().init();
  </script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection