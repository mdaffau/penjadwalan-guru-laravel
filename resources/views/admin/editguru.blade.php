@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
<div class="col-md-12">
						<div class="Compose-Message">
						<div class="panel panel-default">
						<div class="panel-heading">
							Edit Guru
						</div>
						<div class="panel-body panel-body-com-m">
							<div class="alert alert-info">
								Silahkan edit isi form edit guru dibawah
							</div>
							
						<form method="post" action="{{url('/admin/editguru', $odit->idguru)}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="idguru" value="{{$odit->idguru}}">
								<hr>
							<div class="form-group">
								<label class="control-label">Nama Guru : </label>
								<br>
								<input type="text" name="namaguru" class="form-control1 control3" value="{{$odit->namaguru}}">
							</div>
							<div class="form-group">
							<label class="control-label">Jenis Kelamin :</label>
							<br>
									@if($odit->jkguru = 'Laki-Laki')
									<select name="jkguru" id="selector1" class="form-control1">
										<option value="Laki-Laki" checked>Laki-Laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
									@else if($odit->jkguru = 'Perempuan')
									<select name="jkguru" id="selector1" class="form-control1">
										<option value="Laki-Laki">Laki-Laki</option>
										<option value="Perempuan" checked>Perempuan</option>
									</select>
									@endif
							</div>
							<div class="form-group">
								<label class="control-label">Alamat Guru : </label>
								<br>
								<input type="text" name="alamatguru" class="form-control1 control3" value="{{$odit->alamatguru}}">
							</div>

							<div class="form-group">
								<label class="control-label">Email Guru : </label>
								<br>
								<input type="email" name="emailguru" class="form-control1 control3" value="{{$odit->emailguru}}">
							</div>
							<button type="submit" class="btn btn-success">Simpan</button>
							
							<hr>
						</form>
					</div>
					</div>
					</div>
					</div>
</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{ url('databel/jquery.dataTables.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{ url('databel/dataTables.bootstrap.min.js')}}"></script>
<!-- App scripts -->
</body>
</html>
@endsection