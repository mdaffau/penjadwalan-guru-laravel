@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
	<div class="panel panel-info">
  <div class="panel-heading">Profile</div>
  <div class="panel-body">
  <div class="col-md-4">
  <div class="image">
    @if($foto->foto === 0)
      <img src="{{url('template/images/profile.jpg')}}" style="width:80%; height:80%;"></img>
    @else
      <img src="{{url('fotoadmin/'$foto->foto)}}" style="width:80%; height:80%;"></img>
    @endif
    	<hr>
  </div>
  </div>

  <div class="col-md-8">
  <div class="input-profile">
  <center><h3 style="font-family:Haettenschweiler;font-size:40px;">Ganti Foto</h3></center>
  <br>
  <div class="form-group">
    <form method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <label>Pilih Foto</label>
      <input type="file" name="foto" value="{{$foto->foto}}">
      <br>
      <input type="submit" value="Ganti" class="btn btn-success">
      <input type="reset" value="Reset" class="btn btn-danger">
    </form>
  </div>
  </div>
  </div>

  </div>
</div>

</div>
	</div>

				<div class="clearfix"></div>
			</div>
		</div>

		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   <script src="{{ url('template/js/wow.min.js')}}"></script>
   <script>
   new WOW().init();
   </script>
   <script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
   <script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
   <script src="{{ url('template/js/scripts.js')}}"></script>
   <!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>

</body>
</html>
@endsection