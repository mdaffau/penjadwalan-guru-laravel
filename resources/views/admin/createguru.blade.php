@extends('layouts.templateadmin')
@section('content')
<div id="page-wrapper">
<div class="col-md-12">
						<div class="Compose-Message">
						<div class="panel panel-default">
						<div class="panel-heading">
							Tambah Guru
						</div>
						<div class="panel-body panel-body-com-m">
							<div class="alert alert-info">
								Silahkan isi form tambah guru dibawah
							</div>
						<form class="" action="" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
								<hr>
							<div class="form-group">
								<label class="control-label">Nama Guru : </label>
								<br>
								<input type="text" name="namaguru" class="form-control1 control3" value="{{old('namaguru') }}">
								<br>
								<span class="error"><?php echo $errors->first('namaguru') ?></span>
							</div>
							<div class="form-group">
							<label class="control-label">Jenis Kelamin :</label>
							<br>
									<select name="jkguru" id="selector1" class="form-control1" value="{{old('jkguru') }}">
										<option value="Laki-Laki">Laki-Laki</option>
										<option value="Perempuan">Perempuan</option>
									</select>
									<br>
									<span class="error"><?php echo $errors->first('jkguru') ?></span>
							</div>
							<div class="form-group">
								<label class="control-label">Alamat Guru : </label>
								<br>
								<input type="text" name="alamatguru" class="form-control1 control3" value="{{old('alamatguru') }}">
								<br>
								<span class="error"><?php echo $errors->first('alamatguru') ?></span>
							</div>

							<div class="form-group">
								<label class="control-label">Email Guru : </label>
								<br>
								<input type="email" name="emailguru" class="form-control1 control3" value="{{old('emailguru') }}">
								<br>
								<span class="error"><?php echo $errors->first('emailguru') ?></span>
							</div>
							<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
							<input type="reset" name="reset" value="Reset" class="btn btn-danger">
							<hr>
						</form>
					</div>
					</div>
					</div>
					</div>
</div>

				<div class="clearfix"></div>
			</div>
		</div>
			
		</div>
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2015 Fluxy Admin Panel. All Rights Reserved | ReDesign by <a href="http://luwakdev.id/syahru/" target="_blank">Dafuq</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  <script src="{{ url('template/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<script src="{{ url('template/js/jquery-1.10.2.min.js')}}"></script>
<script src="{{ url('template/js/jquery.nicescroll.js')}}"></script>
<script src="{{ url('template/js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="{{ url('template/js/bootstrap.min.js')}}"></script>
</body>
</html>
@endsection