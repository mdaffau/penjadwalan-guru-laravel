-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29 Nov 2016 pada 13.23
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jadwalbaru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `foto`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$SLQK3SFJHB64sUzKefpFqu9PQbS59sB.ZbUkP//ghcaRzn.zHodie', '', 'vGxpKIzdtxmCl79l1sZquEUOYFqyeICyADjzxYji1GDrhP6C8dK6oivqZNh3', '2016-11-29 05:18:33', '2016-11-29 05:22:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dataguru`
--

CREATE TABLE `dataguru` (
  `idguru` int(10) UNSIGNED NOT NULL,
  `namaguru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jkguru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailguru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamatguru` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `dataguru`
--

INSERT INTO `dataguru` (`idguru`, `namaguru`, `jkguru`, `emailguru`, `alamatguru`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, ' ', ' ', ' ', ' ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datahari`
--

CREATE TABLE `datahari` (
  `idhari` int(10) UNSIGNED NOT NULL,
  `namahari` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `datahari`
--

INSERT INTO `datahari` (`idhari`, `namahari`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Senin', NULL, NULL, NULL),
(2, 'Selasa', NULL, NULL, NULL),
(3, 'Rabu', NULL, NULL, NULL),
(4, 'Kamis', NULL, NULL, NULL),
(5, 'Jumat', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datakelas`
--

CREATE TABLE `datakelas` (
  `idkelas` int(10) UNSIGNED NOT NULL,
  `namakelas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `datakelas`
--

INSERT INTO `datakelas` (`idkelas`, `namakelas`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'X RPL 1', NULL, NULL, NULL),
(2, 'X RPL 2', NULL, NULL, NULL),
(3, 'XI RPL 1', NULL, NULL, NULL),
(4, 'XI RPL 2', NULL, NULL, NULL),
(5, 'XII RPL 1', NULL, NULL, NULL),
(6, 'XII RPL 2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datamapel`
--

CREATE TABLE `datamapel` (
  `idmapel` int(10) UNSIGNED NOT NULL,
  `namamapel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `datamapel`
--

INSERT INTO `datamapel` (`idmapel`, `namamapel`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, ' ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dataruang`
--

CREATE TABLE `dataruang` (
  `idruang` int(10) UNSIGNED NOT NULL,
  `namaruang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `dataruang`
--

INSERT INTO `dataruang` (`idruang`, `namaruang`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, ' ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `idjadwal` int(10) UNSIGNED NOT NULL,
  `jamke` int(11) NOT NULL,
  `idhari` int(11) NOT NULL,
  `idkelas` int(11) NOT NULL,
  `idguru` int(11) NOT NULL,
  `idmapel` int(11) NOT NULL,
  `idruang` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`idjadwal`, `jamke`, `idhari`, `idkelas`, `idguru`, `idmapel`, `idruang`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(2, 2, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(3, 3, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(4, 4, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(5, 5, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(6, 6, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(7, 7, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(8, 8, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(9, 9, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(10, 10, 1, 1, 1, 1, 1, NULL, NULL, NULL),
(11, 1, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(12, 2, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(13, 3, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(14, 4, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(15, 5, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(16, 6, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(17, 7, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(18, 8, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(19, 9, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(20, 10, 1, 2, 1, 1, 1, NULL, NULL, NULL),
(21, 1, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(22, 2, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(23, 3, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(24, 4, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(25, 5, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(26, 6, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(27, 7, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(28, 8, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(29, 9, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(30, 10, 1, 3, 1, 1, 1, NULL, NULL, NULL),
(31, 1, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(32, 2, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(33, 3, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(34, 4, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(35, 5, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(36, 6, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(37, 7, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(38, 8, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(39, 9, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(40, 10, 1, 4, 1, 1, 1, NULL, NULL, NULL),
(41, 1, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(42, 2, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(43, 3, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(44, 4, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(45, 5, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(46, 6, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(47, 7, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(48, 8, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(49, 9, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(50, 10, 1, 5, 1, 1, 1, NULL, NULL, NULL),
(51, 1, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(52, 2, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(53, 3, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(54, 4, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(55, 5, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(56, 6, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(57, 7, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(58, 8, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(59, 9, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(60, 10, 1, 6, 1, 1, 1, NULL, NULL, NULL),
(61, 1, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(62, 2, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(63, 3, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(64, 4, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(65, 5, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(66, 6, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(67, 7, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(68, 8, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(69, 9, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(70, 10, 2, 1, 1, 1, 1, NULL, NULL, NULL),
(71, 1, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(72, 2, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(73, 3, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(74, 4, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(75, 5, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(76, 6, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(77, 7, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(78, 8, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(79, 9, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(80, 10, 2, 2, 1, 1, 1, NULL, NULL, NULL),
(81, 1, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(82, 2, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(83, 3, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(84, 4, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(85, 5, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(86, 6, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(87, 7, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(88, 8, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(89, 9, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(90, 10, 2, 3, 1, 1, 1, NULL, NULL, NULL),
(91, 1, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(92, 2, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(93, 3, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(94, 4, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(95, 5, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(96, 6, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(97, 7, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(98, 8, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(99, 9, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(100, 10, 2, 4, 1, 1, 1, NULL, NULL, NULL),
(101, 1, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(102, 2, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(103, 3, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(104, 4, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(105, 5, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(106, 6, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(107, 7, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(108, 8, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(109, 9, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(110, 10, 2, 5, 1, 1, 1, NULL, NULL, NULL),
(111, 1, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(112, 2, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(113, 3, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(114, 4, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(115, 5, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(116, 6, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(117, 7, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(118, 8, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(119, 9, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(120, 10, 2, 6, 1, 1, 1, NULL, NULL, NULL),
(121, 1, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(122, 2, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(123, 3, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(124, 4, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(125, 5, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(126, 6, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(127, 7, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(128, 8, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(129, 9, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(130, 10, 3, 1, 1, 1, 1, NULL, NULL, NULL),
(131, 1, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(132, 2, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(133, 3, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(134, 4, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(135, 5, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(136, 6, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(137, 7, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(138, 8, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(139, 9, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(140, 10, 3, 2, 1, 1, 1, NULL, NULL, NULL),
(141, 1, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(142, 2, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(143, 3, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(144, 4, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(145, 5, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(146, 6, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(147, 7, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(148, 8, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(149, 9, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(150, 10, 3, 3, 1, 1, 1, NULL, NULL, NULL),
(151, 1, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(152, 2, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(153, 3, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(154, 4, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(155, 5, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(156, 6, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(157, 7, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(158, 8, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(159, 9, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(160, 10, 3, 4, 1, 1, 1, NULL, NULL, NULL),
(161, 1, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(162, 2, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(163, 3, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(164, 4, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(165, 5, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(166, 6, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(167, 7, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(168, 8, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(169, 9, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(170, 10, 3, 5, 1, 1, 1, NULL, NULL, NULL),
(171, 1, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(172, 2, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(173, 3, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(174, 4, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(175, 5, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(176, 6, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(177, 7, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(178, 8, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(179, 9, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(180, 10, 3, 6, 1, 1, 1, NULL, NULL, NULL),
(181, 1, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(182, 2, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(183, 3, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(184, 4, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(185, 5, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(186, 6, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(187, 7, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(188, 8, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(189, 9, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(190, 10, 4, 1, 1, 1, 1, NULL, NULL, NULL),
(191, 1, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(192, 2, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(193, 3, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(194, 4, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(195, 5, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(196, 6, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(197, 7, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(198, 8, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(199, 9, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(200, 10, 4, 2, 1, 1, 1, NULL, NULL, NULL),
(201, 1, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(202, 2, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(203, 3, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(204, 4, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(205, 5, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(206, 6, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(207, 7, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(208, 8, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(209, 9, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(210, 10, 4, 3, 1, 1, 1, NULL, NULL, NULL),
(211, 1, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(212, 2, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(213, 3, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(214, 4, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(215, 5, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(216, 6, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(217, 7, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(218, 8, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(219, 9, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(220, 10, 4, 4, 1, 1, 1, NULL, NULL, NULL),
(221, 1, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(222, 2, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(223, 3, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(224, 4, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(225, 5, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(226, 6, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(227, 7, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(228, 8, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(229, 9, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(230, 10, 4, 5, 1, 1, 1, NULL, NULL, NULL),
(231, 1, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(232, 2, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(233, 3, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(234, 4, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(235, 5, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(236, 6, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(237, 7, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(238, 8, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(239, 9, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(240, 10, 4, 6, 1, 1, 1, NULL, NULL, NULL),
(241, 1, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(242, 2, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(243, 3, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(244, 4, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(245, 5, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(246, 6, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(247, 7, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(248, 8, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(249, 9, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(250, 10, 5, 1, 1, 1, 1, NULL, NULL, NULL),
(251, 1, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(252, 2, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(253, 3, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(254, 4, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(255, 5, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(256, 6, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(257, 7, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(258, 8, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(259, 9, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(260, 10, 5, 2, 1, 1, 1, NULL, NULL, NULL),
(261, 1, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(262, 2, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(263, 3, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(264, 4, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(265, 5, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(266, 6, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(267, 7, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(268, 8, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(269, 9, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(270, 10, 5, 3, 1, 1, 1, NULL, NULL, NULL),
(271, 1, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(272, 2, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(273, 3, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(274, 4, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(275, 5, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(276, 6, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(277, 7, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(278, 8, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(279, 9, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(280, 10, 5, 4, 1, 1, 1, NULL, NULL, NULL),
(281, 1, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(282, 2, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(283, 3, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(284, 4, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(285, 5, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(286, 6, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(287, 7, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(288, 8, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(289, 9, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(290, 10, 5, 5, 1, 1, 1, NULL, NULL, NULL),
(291, 1, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(292, 2, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(293, 3, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(294, 4, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(295, 5, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(296, 6, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(297, 7, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(298, 8, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(299, 9, 5, 6, 1, 1, 1, NULL, NULL, NULL),
(300, 10, 5, 6, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_05_11_031938_create_admins_table', 1),
('2016_11_27_084155_ruang_kelas', 1),
('2016_11_29_100556_saran', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pilihan`
--

CREATE TABLE `pilihan` (
  `idpilihan` int(10) UNSIGNED NOT NULL,
  `idhari` int(11) NOT NULL,
  `idkelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `pilihan`
--

INSERT INTO `pilihan` (`idpilihan`, `idhari`, `idkelas`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 2, 1),
(8, 2, 2),
(9, 2, 3),
(10, 2, 4),
(11, 2, 5),
(12, 2, 6),
(13, 3, 1),
(14, 3, 2),
(15, 3, 3),
(16, 3, 4),
(17, 3, 5),
(18, 3, 6),
(19, 4, 1),
(20, 4, 2),
(21, 4, 3),
(22, 4, 4),
(23, 4, 5),
(24, 4, 6),
(25, 5, 1),
(26, 5, 2),
(27, 5, 3),
(28, 5, 4),
(29, 5, 5),
(30, 5, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `saran`
--

CREATE TABLE `saran` (
  `idsaran` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `dataguru`
--
ALTER TABLE `dataguru`
  ADD PRIMARY KEY (`idguru`);

--
-- Indexes for table `datahari`
--
ALTER TABLE `datahari`
  ADD PRIMARY KEY (`idhari`);

--
-- Indexes for table `datakelas`
--
ALTER TABLE `datakelas`
  ADD PRIMARY KEY (`idkelas`);

--
-- Indexes for table `datamapel`
--
ALTER TABLE `datamapel`
  ADD PRIMARY KEY (`idmapel`);

--
-- Indexes for table `dataruang`
--
ALTER TABLE `dataruang`
  ADD PRIMARY KEY (`idruang`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`idjadwal`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pilihan`
--
ALTER TABLE `pilihan`
  ADD PRIMARY KEY (`idpilihan`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`idsaran`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dataguru`
--
ALTER TABLE `dataguru`
  MODIFY `idguru` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `datahari`
--
ALTER TABLE `datahari`
  MODIFY `idhari` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `datakelas`
--
ALTER TABLE `datakelas`
  MODIFY `idkelas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `datamapel`
--
ALTER TABLE `datamapel`
  MODIFY `idmapel` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dataruang`
--
ALTER TABLE `dataruang`
  MODIFY `idruang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `idjadwal` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;
--
-- AUTO_INCREMENT for table `pilihan`
--
ALTER TABLE `pilihan`
  MODIFY `idpilihan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `idsaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
