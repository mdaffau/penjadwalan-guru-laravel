<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dataruang extends Model
{
    protected $table = "dataruang";
    protected $fillable = ['idruang','namaruang'];
    protected $primaryKey = "idruang";   
}
