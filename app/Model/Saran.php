<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Saran extends Model
{
    protected $table = "saran";
    protected $fillable = ['idsaran','nama','email','pesan'];
    protected $primaryKey = "idsaran";   
}
