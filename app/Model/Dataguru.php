<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dataguru extends Model
{
    protected $table = "dataguru";
    protected $fillable = ['idguru','namaguru','jkguru','emailguru','alamatguru'];
    protected $primaryKey = "idguru";
}
