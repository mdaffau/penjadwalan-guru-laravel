<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Datakelas extends Model
{
    protected $table = "datakelas";
    protected $fillable = ['idkelas','namakelas'];
    protected $primaryKey = "idkelas";
}
