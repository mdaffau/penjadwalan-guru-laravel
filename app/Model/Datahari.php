<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Datahari extends Model
{
    protected $table = "datahari";
    protected $fillable = ['idhari','namahari'];
    protected $primaryKey = "idhari";
}
