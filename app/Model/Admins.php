<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admins extends Model
{
    protected $table = "admins";
    protected $fillable = ['id','name','password','foto'];
    protected $primaryKey = "id";
}
