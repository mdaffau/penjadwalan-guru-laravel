<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Datamapel extends Model
{
    protected $table = "datamapel";
    protected $fillable = ['idmapel','namamapel'];
    protected $primaryKey = "idmapel";    
}
