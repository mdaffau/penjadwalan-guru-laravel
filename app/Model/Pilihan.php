<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pilihan extends Model
{
    protected $table = "pilihan";
    protected $fillable = ['idpilihan','idkelas','idhari'];
    protected $primaryKey = "idpilihan";
}
