<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Dataguru;
use App\Model\Datamapel;
use App\Model\Dataruang;
use DB;

class Jadwal extends Model
{
     protected $table = "jadwal";
    protected $fillable = ['idjadwal', 'jamke','idguru','idmapel','idkelas','idhari'];
   protected $primaryKey = "idjadwal";

    public function seninkelas1(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '1' ORDER BY jadwal.jamke asc");
        return $data;
    }
    public function seninkelas2(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '2' ORDER BY jadwal.jamke asc");
        return $data;
    }
    public function seninkelas3(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '3' ORDER BY jadwal.jamke asc");
        return $data;
    }
    public function seninkelas4(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '4' ORDER BY jadwal.jamke asc");
        return $data;
    }
    public function seninkelas5(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '5' ORDER BY jadwal.jamke asc");
        return $data;
    }
    public function seninkelas6(){
        $data = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.idkelas = '6' ORDER BY jadwal.jamke asc");
        return $data;
    }

    public function guru(){
        return $this->belongsTo( Dataguru::class , 'idguru');
    }
    public function mapel(){
        return $this->belongsTo( Datamapel::class , 'idmapel');
    }
    public function kelas(){
        return $this->belongsTo( Datakelas::class , 'idkelas');
    }
    public function ruang(){
        return $this->belongsTo( Dataruang::class , 'idruang');
    }
}
