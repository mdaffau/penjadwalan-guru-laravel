<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Model\Dataguru;
use App\Model\Datamapel;
use App\Model\Datahari;
use App\Model\Datakelas;
use App\Model\Dataruang;
use App\Model\Pilihan;
use App\Model\Jadwal;
use App\Model\Admins;
use App\User;
use App\Admin;
use DB;
use Validator;
use Auth;
use Alert;  

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('admin');
    }

    public function index(){
        return view('admin.index');
    }
    //lihatjadwal
    public function lihat(){
        $hari = Datahari::all();
        return view('admin.lihatjadwal', compact('hari'));
    }
     public function ajaxkelas($idhari){
        if($idhari == 0){
            $kelas = Datakelas::all();
        }else{
            $kelas = DB::select("SELECT * from pilihan INNER JOIN datahari on pilihan.idhari = datahari.idhari INNER JOIN datakelas on pilihan.idkelas = datakelas.idkelas WHERE pilihan.idhari = '$idhari'");
        }return $kelas;
    }
    public function ajaxpilih($idhari, $idkelas){
        if($idhari == 0 && $idkelas == 0){
            $gawe = new Jadwal();
            $jadwal = $gawe->seninkelas1();
        }else{
            $jadwal = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '$idkelas' ORDER BY jadwal.jamke asc");
        }return $jadwal;
    }
    //AturJadwal
    public function pilihhari(){
        $hari = Datahari::all();
        $jadwal = new Jadwal();
        $ambil = $jadwal->seninkelas1();
        $ambil1 = $jadwal->seninkelas2();
        $ambil2 = $jadwal->seninkelas3();
        $ambil3 = $jadwal->seninkelas4();
        $ambil4 = $jadwal->seninkelas5();
        $ambil5 = $jadwal->seninkelas6();
        $guru = DB::select("SELECT * FROM dataguru WHERE dataguru.idguru != '1'");
        $mapel = DB::select("SELECT * FROM datamapel WHERE datamapel.idmapel != '1'");
        $ruang = DB::select("SELECT * FROM dataruang WHERE dataruang.idruang != '1'");
        return view('admin.aturjadwal',compact('hari','ambil','ambil1','ambil2','ambil3','ambil4','ambil5','guru','mapel','ruang'));
    }
    public function ajaxhari($idhari){
        if($idhari == 0){
            $jadwal = new Jadwal();
            $ambil = $jadwal->seninkelas1();
            $ambil1 = $jadwal->seninkelas2();
            $ambil2 = $jadwal->seninkelas3();
            $ambil3 = $jadwal->seninkelas4();
            $ambil4 = $jadwal->seninkelas5();
            $ambil5 = $jadwal->seninkelas6();
        }else{
            $ambil = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '1' ORDER BY jadwal.jamke asc");
            $ambil1 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '2' ORDER BY jadwal.jamke asc");
            $ambil2 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '3' ORDER BY jadwal.jamke asc");
            $ambil3 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '4' ORDER BY jadwal.jamke asc");
            $ambil4 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '5' ORDER BY jadwal.jamke asc");
            $ambil5 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '$idhari' AND jadwal.idkelas = '6' ORDER BY jadwal.jamke asc");
        }return array(['ambil' => $ambil,
                        'ambil1' => $ambil1, 'ambil2' => $ambil2, 'ambil3' => $ambil3, 'ambil4' => $ambil4, 'ambil5' => $ambil5]);
    }
    //Guru
    public function daftarguru(){
    	$dataguru = DB::select("SELECT * FROM dataguru WHERE dataguru.idguru != '1'");
        // dd($dataguru);
    	return view('admin.daftarguru', compact('dataguru'));
    }
    public function createguru(){
        return view('admin.createguru');
    }
    public function buatguru(Request $request){
        $validator = $this->validator();
        if ($validator->fails()) {
            return redirect('/admin/createguru')
            ->withErrors($validator)
            ->withInput();
        }
        $buat = new Dataguru();
        $buat ->namaguru = request()->namaguru;
        $buat ->jkguru = request()->jkguru;
        $buat ->alamatguru = request()->alamatguru;
        $buat ->emailguru = request()->emailguru;
        $done = $buat->save();
        if($done > 0){
            $akun = new User();
            $akun ->email = $buat->emailguru;
            $akun ->password = Hash::make('1234567890');
            $akun ->name = $buat->namaguru;
            $kun = $akun->save();
            if($kun > 0){
                Alert::success('Success Message', 'Optional Title');
                return redirect('/admin/daftarguru');
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back(); 
        }
    }
    public function validator() {
    $messages = [
            'required' => 'Data belum diisi!',
            'unique' => 'Data sudah ada!.',
        ];
        return Validator::make(request()->all(), [
            'namaguru' => 'required|unique:dataguru,namaguru|max:255',
            'jkguru' => 'required',
            'alamatguru' => 'required',
            'emailguru' => 'required|unique:dataguru,emailguru|max:255',
        ],$messages);
    }
    public function editguru($idguru){
        $odit = Dataguru::findOrFail($idguru);
        // dd($edit);
        return view('admin.editguru', compact('odit'));
    }
    public function updateguru(Request $request, $idguru) {
        $update = Dataguru::findOrFail($idguru);
        $update->namaguru = request()->namaguru;
        $update->jkguru = request()->jkguru;
        $update->alamatguru = request()->alamatguru;
        $update->emailguru = request()->emailguru;
        $update->update();

        return redirect()->to('/admin/daftarguru');
    }
    public function deleteguru($idguru){
        $delete = Dataguru::findOrFail($idguru);
        $delete->delete();

        return redirect('/admin/daftarguru');
    }

    //Mapel
    public function daftarmapel(){
        $datamapel = DB::select("SELECT * FROM datamapel WHERE idmapel != '1'");
    	return view('admin.daftarmapel', compact('datamapel'));
    }
    public function createmapel(){
        return view('admin.createmapel');
    }
    public function buatmapel(Request $request){
        $validator = $this->validator1();
        if ($validator->fails()) {
            return redirect('/admin/createmapel')
            ->withErrors($validator)
            ->withInput();
        }
        $buat = new Datamapel();
        $buat ->namamapel = request()->namamapel;
        $done = $buat->save();
        if($done > 0){
            return redirect('/admin/daftarmapel');
        }else{
            return redirect()->back(); 
        }
    }
    public function validator1() {
    $messages = [
            'required' => 'Data belum diisi!',
            'unique' => 'Data sudah ada!',
        ];
        return Validator::make(request()->all(), [
            'namamapel' => 'required|unique:datamapel,namamapel|max:255',
        ],$messages);
    }
    public function editmapel($idmapel){
        $odit = Datamapel::findOrFail($idmapel);
        return view('admin.editmapel', compact('odit'));
    }
    public function updatemapel(Request $request, $idmapel) {
        $update = Datamapel::findOrFail($idmapel);
        $update->namamapel = request()->namamapel;
        $update->update();

        return redirect()->to('/admin/daftarmapel');
    }
    public function deletemapel($idmapel){
        $delete = Datamapel::findOrFail($idmapel);
        $delete->delete();

        return redirect()->back();
    }
    public function profile(){
        $tampil = DB::select("SELECT * FROM admins WHERE admins.id = '".Auth::guard('admin')->user()->id."'");
        return view('admin.profile',compact('tampil'));
    }
    public function gantifoto($id){
        $foto = Admins::findOrFail($id);
        // dd($foto->foto);
        return view('admin.gantifoto',compact('foto'));
    }
    public function changefoto(Request $request, $id){
        $imageTempName = $request->file('foto')->getPathname();
        $imageName = $request->file('foto')->getClientOriginalName();
        $path = base_path() . '/public/fotoadmin';
        $request->file('foto')->move($path , $imageName);
        $foto = Admins::findOrFail($id);    
        $foto->foto = $imageName;
        $done = $foto->save();
        if($done > 0){
            return redirect('/admin/profile')->with('message','Berhasil mengganti foto!');
        }else{
            return redirect()->back()->with('messages','Gagal mengganti foto');
        }
    }
    public function cekpassword($id){
        $password = Admins::findOrFail($id);
        return view('admin.cekpassword',compact('password'));
    }
    public function gantipassword(Request $request, $id){
        $validator = $this->validpassword();
        if ($validator->fails()) {
            return redirect('/admin/gantipassword/{id}')
            ->withErrors($validator)
            ->withInput();
        }
        $ganti = Admins::findOrFail($id);
        $pass = request()->password;
        $ganti->password = Hash::make($pass);
        $genti = $ganti->save();
        if($genti > 0){
            return redirect('/admin/profile')->with('message','Berhasil mengganti password!');
        }else{
            return redirect()->back()->with('messages','Gagal mengganti password');
        }
    }
    public function validpassword() {
    $messages = [
            'required' => 'Data belum diisi!',
        ];
        return Validator::make(request()->all(), [
            'password' => 'required|max:255',
        ],$messages);
    }

    //Ruang
    public function daftarruang(){
        $dataruang = DB::select("SELECT * FROM dataruang WHERE idruang != '1'");
        return view('admin.daftarruang', compact('dataruang'));
    }
    public function createruang(){
        return view('admin.createruang');
    }
    public function buatruang(Request $request){
        $validator = $this->validator2();
        if ($validator->fails()) {
            return redirect('/admin/createruang')
            ->withErrors($validator)
            ->withInput();
        }
        $buat = new Dataruang();
        $buat ->namaruang = request()->namaruang;
        $done = $buat->save();
        if($done > 0){
            return redirect('/admin/daftarruang');
        }else{
            return redirect()->back(); 
        }
    }
    public function validator2() {
    $messages = [
            'required' => 'Data belum diisi!',
            'unique' => 'Data sudah ada!',
        ];
        return Validator::make(request()->all(), [
            'namaruang' => 'required|unique:dataruang,namaruang|max:255',
        ],$messages);
    }
    public function editruang($idruang){
        $odit = Dataruang::findOrFail($idruang);
        return view('admin.editruang', compact('odit'));
    }
    public function updateruang(Request $request, $idruang) {
        $update = Dataruang::findOrFail($idruang);
        $update->namaruang = request()->namaruang;
        $update->update();

        return redirect()->to('/admin/daftarruang');
    }
    public function deleteruang($idruang){
        $delete = Dataruang::findOrFail($idruang);
        $delete->delete();

        return redirect()->back();
    }

    //Statis
    public function panduatur(){
        return view('admin.panduan.aturjadwal');
    }
    public function pandulihat(){
        return view('admin.panduan.lihatjadwal');
    }
    public function panduguru(){
        return view('admin.panduan.guru');
    }
    public function pandumapel(){
        return view('admin.panduan.mapel');
    }
    public function panduruang(){
        return view('admin.panduan.ruang');
    }
}

