<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Saran;

class IndexController extends Controller
{
    public function __construct(){
        $this->middleware('guest');
    }
    public function index(){
    	return view('index');
    }
    public function login(){
    	return view('welcome');
    }
    public function saran(){
    	$saran = new Saran();
    	$saran->nama = request()->nama;
    	$saran->email = request()->email;
    	$saran->pesan = request()->pesan;
    	$berhasil = $saran->save();
    	if($berhasil > 0){
    		return redirect('/');
    	}else{
    		return redirect('/');
    	}
    }
}
