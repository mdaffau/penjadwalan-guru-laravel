<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Dataguru;
use App\Model\Datamapel;
use App\Model\Datahari;
use App\Model\Datakelas;
use App\Model\Dataruang;
use App\Model\Pilihan;
use App\Model\Jadwal;
use App\Model\Users;
use DB;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        return view('user.index');
    }
    public function today(){
		date_default_timezone_set('Asia/Jakarta');
		$a_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$hari = $a_hari[date("N")];
        $tampil = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND dataguru.namaguru = '".Auth::user()->name."'");
		$tampil1 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil2 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil3 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil4 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil5 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil6 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil7 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil8 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil9 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil10 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE datahari.namahari = '$hari' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");
		return view('user.jadwaltoday',compact('tampil','tampil1','tampil2','tampil3','tampil4','tampil5','tampil6','tampil7','tampil8','tampil9','tampil10'));
    }
    public function perhari(){
        $tampil = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE dataguru.namaguru = '".Auth::user()->name."'");
        //Senin//
        $tampil11 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil12 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil13 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil14 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil15 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil16 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil17 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil18 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil19 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil20 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '1' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");


        //Selasa//
        $tampil21 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil22 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil23 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil24 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil25 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil26 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil27 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil28 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil29 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil30 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '2' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");


        //Rabu//
        $tampil31 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil32 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil33 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil34 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil35 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil36 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil37 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil38 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil39 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil40 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '3' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");


        //Kamis//
        $tampil41 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil42 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil43 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil44 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil45 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil46 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil47 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil48 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil49 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil50 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '4' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");

        //Jumat//
        $tampil51 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '1' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil52 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '2' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil53 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '3' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil54 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '4' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil55 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '5' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil56 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '6' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil57 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '7' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil58 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '8' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil59 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '9' AND dataguru.namaguru = '".Auth::user()->name."'");
        $tampil60 = DB::select("SELECT * from jadwal INNER JOIN datahari on jadwal.idhari = datahari.idhari INNER JOIN datakelas on jadwal.idkelas = datakelas.idkelas INNER JOIN dataguru on jadwal.idguru = dataguru.idguru INNER JOIN datamapel on jadwal.idmapel = datamapel.idmapel INNER JOIN dataruang on jadwal.idruang = dataruang.idruang WHERE jadwal.idhari = '5' AND jadwal.jamke = '10' AND dataguru.namaguru = '".Auth::user()->name."'");
        return view('user.jadwalperhari',compact('tampil','tampil11','tampil12','tampil13','tampil14','tampil15','tampil16','tampil17','tampil18','tampil19','tampil20','tampil21','tampil22','tampil23','tampil24','tampil25','tampil26','tampil27','tampil28','tampil29','tampil30','tampil31','tampil32','tampil33','tampil34','tampil35','tampil36','tampil37','tampil38','tampil39','tampil40','tampil41','tampil42','tampil43','tampil44','tampil45','tampil46','tampil47','tampil48','tampil49','tampil50','tampil51','tampil52','tampil53','tampil54','tampil55','tampil56','tampil57','tampil58','tampil59','tampil60'));
    }
    public function profile(){
        $tampil = DB::select("SELECT * FROM users WHERE users.id = '".Auth::user()->id."'");
        return view('user.profile',compact('tampil'));
    }
    public function gantifoto($id){
        $foto = Users::findOrFail($id);
        // dd($foto->foto);
        return view('user.gantifoto',compact('foto'));
    }
    public function changefoto(Request $request, $id){
        $imageTempName = $request->file('foto')->getPathname();
        $imageName = $request->file('foto')->getClientOriginalName();
        $path = base_path() . '/public/fotouser';
        $request->file('foto')->move($path , $imageName);
        $foto = Users::findOrFail($id);    
        $foto->foto = $imageName;
        $done = $foto->save();
        if($done > 0){
            return redirect('/user/profile')->with(array('message' => 'Foto berhasil diganti!'));
        }else{
            return redirect()->back()->with(array('message' => 'Gagal mengganti foto!'));
        }
    }
    public function cekpassword($id){
        $password = Users::findOrFail($id);
        return view('user.cekpassword',compact('password'));
    }
    public function gantipassword(Request $request, $id){
        $validator = $this->validpassword();
        if ($validator->fails()) {
            return redirect('/user/gantipassword/{id}')
            ->withErrors($validator)
            ->withInput();
        }
        $ganti = Users::findOrFail($id);
        $pass = request()->password;
        $ganti->password = Hash::make($pass);
        $genti = $ganti->save();
        if($genti > 0){
            return redirect('/user/profile')->with('message','Berhasil mengganti password!');
        }else{
            return redirect()->back()->with('messages','Gagal mengganti password');
        }
    }
    public function validpassword() {
    $messages = [
            'required' => 'Data belum diisi!',
        ];
        return Validator::make(request()->all(), [
            'password' => 'required|max:255',
        ],$messages);
    }
    //Panduan//
    public function panduanall(){
        return view('user.panduan.jadwalall');
    }
    public function panduannow(){
        return view('user.panduan.jadwalnow');
    }
}
