<?php
use App\Model\Dataguru;
use App\Model\Datamapel;
use App\Model\Datahari;
use App\Model\Datakelas;
use App\Model\Dataruang;
use App\Model\Jadwal;
use Illuminate\Http\Request;
use App\Http\Requests;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','IndexController@index');
Route::post('/','IndexController@saran');
Route::get('/index','IndexController@login');

Route::auth();
Route::get('/user', 'UserController@index');
Route::get('/user/jadwalnow', 'UserController@today');
Route::get('/user/jadwalperhari','UserController@perhari');
Route::get('/user/profile','UserController@profile');
Route::get('/user/gantifoto/{id}','UserController@gantifoto');
Route::post('/user/gantifoto/{id}','UserController@changefoto');
Route::get('/user/gantipassword/{id}','UserController@cekpassword');
Route::post('/user/gantipassword/{id}','UserController@gantipassword');
//Panduan
Route::get('/user/panduan/jadwalnow','UserController@panduannow');
Route::get('/user/panduan/jadwalall','UserController@panduanall');

Route::get('/admin','AdminController@index');
Route::get('/admin/login','AdminAuth\AuthController@showLoginForm');
Route::post('/admin/login','AdminAuth\AuthController@login');
Route::get('/admin/logout','AdminAuth\AuthController@logout');

Route::get('/admin/register','AdminAuth\AuthController@showRegisterForm');
Route::post('/admin/register', 'AdminAuth\AuthController@register');

Route::get('/admin/lihatjadwal', 'AdminController@lihat');
Route::get('/admin/lihatjadwal/{idhari}', 'AdminController@ajaxkelas');
Route::get('/admin/lihatjadwal/{idhari}/{idkelas}', 'AdminController@ajaxpilih');
//AturJadwal
Route::get('/admin/aturjadwal', 'AdminController@pilihhari');
Route::get('/admin/aturjadwal/{idhari}', 'AdminController@ajaxhari');
Route::get('/admin/aturjadwal/edit/{idjadwal}',function($idjadwal){
		    $task = Jadwal::with('guru','mapel','ruang')->find($idjadwal);
		    // dd($task->with('guru')->get());
		    return response()->json($task);
});
Route::put('/admin/aturjadwal/edit/{idjadwal}',function(Request $request, $idjadwal){
			$task = Jadwal::find($idjadwal);

			$check = Jadwal::where('idkelas', '!=', $task->idkelas)->where('idhari', $task->idhari)->where('idguru', $request->idguru)->where('jamke', $task->jamke)->get();

			$eror = Jadwal::where('idkelas', $task->idkelas)->where('idhari', $task->idhari)->where('idguru', $request->idguru)->where('idjadwal', '!=', $idjadwal)->get();
			

			if($check->count() > 0){
				return Response::json(array('message' => 'Guru tidak boleh sama! Pada jam pelajaran ke '. $task->jamke));
			}else if($eror->count() > 3){
				return Response::json(array('message' => 'Jadwal guru mengajar pada '. $task->jamke . ' tidak boleh lebih dari 4'));
			}

			$task->idguru = $request->idguru;
			$task->idmapel = $request->idmapel;
			$task->idruang = $request->idruang;

			$task->update();

			return Response::json(Jadwal::with('guru','mapel','ruang')->find($idjadwal));
		
});
//Daftarguru
Route::get('/admin/daftarguru', 'AdminController@daftarguru');
Route::get('/admin/createguru','AdminController@createguru');
Route::post('/admin/createguru','AdminController@buatguru');
Route::get('/admin/editguru/{idguru}','AdminController@editguru');
Route::post('/admin/editguru/{idguru}','AdminController@updateguru');
Route::get('/admin/deleteguru/{idguru}','AdminController@deleteguru');
//Daftarmapel
Route::get('/admin/daftarmapel', 'AdminController@daftarmapel');
Route::get('/admin/createmapel','AdminController@createmapel');
Route::post('/admin/createmapel','AdminController@buatmapel');
Route::get('/admin/editmapel/{idmapel}','AdminController@editmapel');
Route::post('/admin/editmapel/{idmapel}','AdminController@updatemapel');
Route::get('/admin/deletemapel/{idmapel}','AdminController@deletemapel');
//Daftarruang
Route::get('/admin/daftarruang', 'AdminController@daftarruang');
Route::get('/admin/createruang','AdminController@createruang');
Route::post('/admin/createruang','AdminController@buatruang');
Route::get('/admin/editruang/{idruang}','AdminController@editruang');
Route::post('/admin/editruang/{idruang}','AdminController@updateruang');
Route::get('/admin/deleteruang/{idruang}','AdminController@deleteruang');
//Profile
Route::get('/admin/profile','AdminController@profile');
Route::get('/admin/gantifoto/{id}','AdminController@gantifoto');
Route::post('/admin/gantifoto/{id}','AdminController@changefoto');
Route::get('/admin/gantipassword/{id}','AdminController@cekpassword');
Route::post('/admin/gantipassword/{id}','AdminController@gantipassword');
//Statis Panduan
Route::get('/admin/panduan/aturjadwal','AdminController@panduatur');
Route::get('/admin/panduan/lihatjadwal','AdminController@pandulihat');
Route::get('/admin/panduan/guru','AdminController@panduguru');
Route::get('/admin/panduan/mapel','AdminController@pandumapel');
Route::get('/admin/panduan/ruang','AdminController@panduruang');