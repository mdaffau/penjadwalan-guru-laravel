<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RuangKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataguru', function (Blueprint $table) {
            $table->increments('idguru');
            $table->string('namaguru');
            $table->string('jkguru');
            $table->string('emailguru');
            $table->string('alamatguru');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('datamapel', function (Blueprint $table) {
            $table->increments('idmapel');
            $table->string('namamapel');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('datahari', function (Blueprint $table) {
            $table->increments('idhari');
            $table->string('namahari');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('datakelas', function (Blueprint $table) {
            $table->increments('idkelas');
            $table->string('namakelas');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('jadwal', function (Blueprint $table) {
            $table->increments('idjadwal');
            $table->integer('jamke');
            $table->integer('idhari');
            $table->integer('idkelas');
            $table->integer('idguru');
            $table->integer('idmapel');
            $table->integer('idruang');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('pilihan', function (Blueprint $table){
            $table->increments('idpilihan');
            $table->integer('idhari');
            $table->integer('idkelas');
        });
        Schema::create('dataruang', function (Blueprint $table){
            $table->increments('idruang');
            $table->string('namaruang');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dataguru');
        Schema::drop('datamapel');
        Schema::drop('datahari');
        Schema::drop('datakelas');
        Schema::drop('jadwal');
        Schema::drop('pilihan');
        Schema::drop('dataruang');
    }
}
